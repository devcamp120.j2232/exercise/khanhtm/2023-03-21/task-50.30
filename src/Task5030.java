
import java.util.Arrays;
public class Task5030 {
    public static void main(String[] args) {

        //task1
        Object value1 = "Devcamp";
        Object value2 = 1;

        if (value1 instanceof String) {
            System.out.println(value1 + " is a string");
        } else {
            System.out.println(value1 + " is not a string");
        }

        if (value2 instanceof String) {
            System.out.println(value2 + " is a string");
        } else {
            System.out.println(value2 + " is not a string");
        }

        //task2
        String str = "Robin Singh";
        int n = 4;

        String result = str.substring(0, n);

        System.out.println(result);

        //task 3
        String str3 = "Robin Singh";

        String[] words = str3.split(" ");

        System.out.println(Arrays.toString(words));

        //task 4
        String str4 = "Robin Singh from USA";

        // Đổi tất cả các ký tự thành ký tự thường
        String lowerCaseStr = str4.toLowerCase();

        // Thay thế khoảng trắng bằng dấu gạch ngang
        String result4 = lowerCaseStr.replace(" ", "-");

        // In ra kết quả
        System.out.println(result4);


        //task5
        String str51 = "JavaScript Exercises";
        String str52 = "JavaScript exercises";
        String str53 = "JavaScriptExercises";

        String result51 = Task5(str51.replaceAll("\\s+", ""));
        String result52 = Task5(str52.replaceAll("\\s+", ""));
        String result53 = Task5(str53);

        System.out.println(result51);
        System.out.println(result52);
        System.out.println(result53);

        //task6
        String str6 = "js string exercises";
        boolean capitalizeNextChar = true;
        StringBuilder sb = new StringBuilder();
    
        for (char c : str6.toCharArray()) {
          if (capitalizeNextChar && Character.isLetter(c)) {
            c = Character.toUpperCase(c);
            capitalizeNextChar = false;
          } else if (Character.isWhitespace(c)) {
            capitalizeNextChar = true;
          }
          sb.append(c);
        }
    
        String result6 = sb.toString();
        System.out.println(result6);

        //task7
        String word = "Ha!";
    int n71 = 1;
    int n72 = 2;
    int n73 = 3;

    String result71 = Task7(word, n71);
    System.out.println(result71);

    String result72 = Task7(word, n72);
    System.out.println(result72);

    String result73 = Task7(word, n73);
    System.out.println(result73);

    //task8
    String str8 = "dcresource";
    int n81 = 2;
    int n82 = 3;

    String[] arr81 = Task8(str8, n81);
    System.out.println(Arrays.toString(arr81));

    String[] arr82 = Task8(str8, n82);
    System.out.println(Arrays.toString(arr82));

    //task9
    String str9 = "The quick brown fox jumps over the lazy dog";
    String substr9 = "the";

    int count = Task9(str9, substr9);
    System.out.println("The substring '" + substr9 + "' appears " + count + " times in the string.");

    //task10
    String str10 = "00000";
    String replacement = "123";

    int n10 = 4; // số ký tự bên phải cần thay thế
    String newStr = Task10(str10, replacement, n10);

    System.out.println(newStr); // in ra "0000123"
    }

    public static String Task5(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        StringBuilder result = new StringBuilder();
        boolean capitalize = true;
        for (char c : str.toCharArray()) {
            if (Character.isWhitespace(c)) {
                capitalize = true;
            } else if (capitalize) {
                result.append(Character.toUpperCase(c));
                capitalize = false;
            } else {
                result.append(Character.toLowerCase(c));
            }
        }
        return result.toString();
    }

    public static String Task7(String word, int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
          sb.append(word);
          if (i != n - 1) {
            sb.append(" ");
          }
        }
        return sb.toString();
    }

    public static String[] Task8(String str, int n) {
        int length = (int) Math.ceil((double) str.length() / n);
        String[] arr = new String[length];
        for (int i = 0; i < length; i++) {
          int startIndex = i * n;
          int endIndex = Math.min((i + 1) * n, str.length());
          arr[i] = str.substring(startIndex, endIndex);
        }
        return arr;
      }

      public static int Task9(String str, String substr) {
        String[] words = str.split("\\s+");
        int count = 0;
        for (String word : words) {
          if (word.equalsIgnoreCase(substr)) {
            count++;
          }
        }
        return count;
      }

      public static String Task10(String str, String replacement, int n) {
        return str.substring(0, str.length() - n) + replacement;
      }
}
